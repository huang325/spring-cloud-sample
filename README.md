This is a Spring Cloud Sample Project.
Author: Yuzhe Huang

Description:
This project has 3 components, configure server, consumer and producer.
The configure server is helping providing external configure properties for consumer and producer. This will help in the future when are promoting to a distributed system where exists muiltiple consumers and producers working on different regions. By using configure server, we dont need to change code according to machine or regions.

The producer is actually a simple web crawler which will go though the url provided in parameter of request and record data into file assigned by configure file. (The maximum level of crawl is also provided in configure file)

The consumer is only for getting data. It will load the producer stats from te file system and send back whenever you hit the endpoint.




Environment Requirement:
1. JAVA 8
2. Maven




Before start:
Go to src/main/resources/common-config in configure server. In consumer.properties and producer.properties:

1. Changing both paths, Where "D:\\Crawler.txt" by default, to be your choice of file path. We use this location to store our data. (Using File system for now but can be transferred to database).

2. Changing port number to whatever available if needed.Port number should be unique. (This operation is optional)





Set up service:

1. Compile and serve configure server as a normal Maven project.

2. Compile and serve producer app; Hit endpoint at http://localhost:portNumber/?domain=any-url (e.g. http://localhost:8080/?domain=https://spring.io/) ; Wait till result shows up (Number of fetch on any-url: #) 

3. Compile and serve consumer app; Hit endpoint at http://localhost:portNumber/ (e.g. http://localhost:9090/); You will see the content inside the file where you set the path to store data.





