package com.example.consumer.endpoint;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CrawlerDataAPI {
	@Value("${spring.application.path}")
	private String path;
	@RequestMapping(value = "/")
	public String getCrawlerStat() {
			  
		File file = new File(path); 
		List<String> urls = new ArrayList<String>();
	  	String url;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file)); 
		  
			while ((url = br.readLine()) != null) {
				urls.add(url);
			}
			    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		  
	  
      return urls.toString();
	}

}
