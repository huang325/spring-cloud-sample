package com.example.producer.model;

import java.util.ArrayList;
import java.util.List;

public class CrawlStatModel {
	private List<String> urls;
	
	public CrawlStatModel(){
		urls = new ArrayList<String>();
	}

	public List<String> getUrls() {
		return urls;
	}

	public void setUrls(List<String> urls) {
		this.urls = urls;
	}
	
	public void addUrl(String url) {
		this.urls.add(url);
	}
	
	public void addUrls(List<String> urls) {
		this.urls.addAll(urls);
	}
	
	public void merge(CrawlStatModel stat) {
		this.urls.addAll(stat.getUrls());
	}
}
