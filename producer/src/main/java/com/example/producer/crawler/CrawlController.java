package com.example.producer.crawler;

import java.io.File;
import java.util.List;

import com.example.producer.model.CrawlStatModel;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class CrawlController {
	private edu.uci.ics.crawler4j.crawler.CrawlController controller;

    public CrawlController(int maxFetchNum, int maxDepthOfCrawling, String domain) throws Exception {
        
		// Set root dir
		File directory = new File("");
        String rootFolder = directory.getAbsolutePath();
        
        
        // need to be constant
        int numberOfCrawlers = 5;
        int politeness = 200;
        String userAgentString = "vincent";

        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(rootFolder);
        config.setMaxPagesToFetch(maxFetchNum);
        config.setPolitenessDelay(politeness);
        config.setIncludeHttpsPages(true);
        config.setIncludeBinaryContentInCrawling(true);
        config.setMaxDepthOfCrawling(maxDepthOfCrawling);
        config.setUserAgentString(userAgentString);

        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        this.controller = new edu.uci.ics.crawler4j.crawler.CrawlController(config, pageFetcher, robotstxtServer);

        controller.addSeed(domain);
        if(domain.contains("https")) {
			controller.addSeed(domain.replaceFirst("https", "http"));
		} else {
			controller.addSeed(domain.replaceFirst("http", "https"));
		}
        controller.setCustomData(domain);
        controller.startNonBlocking(Crawler.class, numberOfCrawlers);
        
       
    }
    
    public CrawlStatModel extractCrawlData() {
    	this.controller.waitUntilFinish();

        List<Object> crawlersLocalData = controller.getCrawlersLocalData();
        CrawlStatModel totalCrawlerData = new CrawlStatModel();
        for (Object localData : crawlersLocalData) {
            CrawlStatModel stat = (CrawlStatModel) localData;
            totalCrawlerData.merge(stat);
        }
    	return totalCrawlerData;
    }
}
