package com.example.producer.crawler;

import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.http.HttpStatus;

import com.example.producer.model.CrawlStatModel;

import edu.uci.ics.crawler4j.crawler.Page;

public class Crawler extends WebCrawler {
	
	
	private static final Pattern filePatterns = Pattern.compile(".*(\\.(doc|pdf|bmp|gif|jpe?g|png|tiff?))$");
	
	private static Set<String> fileTypeSet;
	
	private CrawlStatModel crawlStat;
	
	public Crawler() {
		crawlStat = new CrawlStatModel();
	}
	
	@Override
    public void onStart() {
		String givenDomain = (String) myController.getCustomData();
		
		fileTypeSet = new HashSet<String>();
		fileTypeSet.add("text/html");
		fileTypeSet.add("image/gif");
		fileTypeSet.add("image/png");
		fileTypeSet.add("image/jpeg");
		fileTypeSet.add("application/pdf");
    }
	
	
	@Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
		String href = url.getURL().toLowerCase();
		
		
		if (filePatterns.matcher(href).matches() || referringPage.getStatusCode() >= 300  || (href.endsWith("/") && referringPage.getContentType() != null && fileTypeSet.contains(getShortTypeName(referringPage.getContentType())))) {
            return true;
        } else {
        	return false;
        }
		
    }
	
	@Override
    public void visit(Page page) {
		crawlStat.addUrl(page.getWebURL().getURL());
    }
	

	@Override
    public Object getMyLocalData() {
        return crawlStat;
    }

	
	private String getShortTypeName(String longName) {
		return longName.substring(0, longName.indexOf(";") == -1 ? longName.length() : longName.indexOf(";"));
	}
}
