package com.example.producer.ulti;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class DataHelper {
	public static void createFile(String path) {
		File file = new File(path);
    	try {
	    	if(file.exists()) {
	    		file.delete();
	    	}
	    	file.createNewFile();
	    	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Failed to create files");
			e.printStackTrace();
		}
    }
	
	public static void recordCrawlerStat(List<String> urls, String path) {
		File file = new File(path);
		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(file);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			for(String url : urls) {
				printWriter.println(url);
			}
		    printWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}

}
