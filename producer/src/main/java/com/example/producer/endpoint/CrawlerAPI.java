package com.example.producer.endpoint;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.producer.crawler.CrawlController;
import com.example.producer.ulti.DataHelper;
@RestController
public class CrawlerAPI {
	@Value("${producer.deepth:2}")
	private int deepth;
	@Value("${producer.pageNum:10}")
	private int pageNum;
	@Value("${producer.path}")
	private String path;
	
	@RequestMapping(value = "/")
	public String StartCrawler(@RequestParam(value="domain", defaultValue="https://www.google.ca/") String domain) {
		CrawlController controller = null;
		try {
			controller = new CrawlController(pageNum, deepth, domain);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	
    	// Get Crawler data
		DataHelper.createFile(path);
		DataHelper.recordCrawlerStat(controller.extractCrawlData().getUrls(), path);
    	
		for(String url : controller.extractCrawlData().getUrls()) {
			System.out.println(url);
		}
		
		return "Number of fetch on "+ domain +" : " + controller.extractCrawlData().getUrls().size();
	}
	

}
